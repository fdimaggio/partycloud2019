const mongoose = require('mongoose');
mongoose.connect('mongodb://admin:1234rewq@ds145555.mlab.com:45555/partycloud', { //  mongodb://admin:1234rewq@ds249503.mlab.com:49503/partycloud
    useNewUrlParser: true
});

const User = mongoose.model('User', {
    name: String,
    fbId: Number,
    profilePic: String,
    codice: String,
    houses: Array,
    userText: String
});

const House = mongoose.model('House', {
    id: String,
    name: String,
    DIMENSIONI: String,
    TARGET: String,
    BONUS: String,
    PUNTEGGIO: Number,
    UBICAZIONE: String,
    ALTEZZA: String,
    DENOMINAZIONE: String,
});

module.exports = {
    saveUser: async function (name, fbId, profilePic, codice, houses, userText) {
        return new Promise(async (resolve, reject) => {
            new User({
                name,
                fbId,
                profilePic,
                codice,
                houses,
                userText
            }).save();
            resolve();
        })
    },
    retriveUser: function (codice) {
        return new Promise(async (resolve, reject) => {
            let user = await User.findOne({
                codice
            });
            resolve(user);
        })
    },
    async pushCasaDB(singolaCasa) {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                // Salva il singolo elemento della collection
                await new House(singolaCasa).save();
                resolve();
            }, 1000);
        })
    },
    async caricaDatabase(listaCase) {
        for (let singleCasa of listaCase) {
            await pushCasaDB(singleCasa)
        }
    },
    async retriveHouses() {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                // Ritorna tutti gli elementi della collection
                resolve(await House.find());
            }, 1000);
        })
    },
    async ottieniDestinazioni() {
        return await retriveHouses();
    }
}