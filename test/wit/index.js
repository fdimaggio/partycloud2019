const AudioContext = require('web-audio-api').AudioContext;
const audioContext = new AudioContext;

const BaseAudioContext = require("base-audio-context");
const toWav = require('audiobuffer-to-wav');
const axios = require("axios");

const mainUrl = "https://cdn.fbsbx.com/v/t59.3654-21/52848295_779134032451609_4420910430201839616_n.aac/audioclip-1552896863212-1675.aac?_nc_cat=106&_nc_ht=cdn.fbsbx.com&oh=66e585c5e26c4c18e4378edd5625449e&oe=5C90F96D"

async function getFileViaUrl(url) {
    return axios.get(url)
}

async function generateAudioData(toConvert) {
    return new Promise((resolve, reject) => {
        audioContext.decodeAudioData(toConvert, buffer => {
            resolve(buffer)
        }, error => {
            reject(error);
        })
    })
}

async function generateAudioData2(toConvert) {
    return new Promise((resolve, reject) => {
        let buf = new Buffer(toConvert);
        resolve(toWav(toConvert));
    })

}

async function run() {
    let {
        data
    } = await getFileViaUrl(mainUrl);
    //console.log(data)
    //let buffer = generateAudioData(data);
    let buffer2 = await generateAudioData2(data);
    console.log(buffer2)
}

run();

/**
 * 
 * request({
     method: "GET",
     url: audio,
     encoding: null
 }, function (error, response, body) {
     audioContext.decodeAudioData(body, buffer => {
         let wav = new Buffer(toWav(buffer));
         console.log(wav);
         options = {
             method: 'POST',
             url: config.witai.speechToText.url,
             qs: {
                 v: config.witai.speechToText.version
             },
             headers: {
                 'content-type': 'audio/wav',
                 authorization: config.witai.speechToText.authentication
             },
             body: wav
         };
         request(options, function (dueError, dueResponse, dueBody) {
             if (JSON.parse(dueBody).error) {
                 console.log("BAD REQUEST!!!");
                 mainRes.send({
                     "messages": [{
                             "text": "Non ho capito bene.."
                         },
                         {
                             "attachment": {
                                 "type": "template",
                                 "payload": {
                                     "template_type": "button",
                                     "text": "Premi qui per inviare un nuovo audio!",
                                     "buttons": [{
                                         "type": "show_block",
                                         "block_names": ["audio"],
                                         "title": "Riprova"
                                     }]
                                 }
                             }
                         }
                     ]
                 });
             } else {
                 console.log(JSON.parse(dueBody));
                 console.log(JSON.parse(dueBody)._text);
                 methods.discoveryChatbot(JSON.parse(dueBody)._text, logger, mainRes, function (discoveryResult, personalityCode) {

                     methods.translateChatbot(JSON.parse(dueBody)._text, logger, mainRes, function (translatedText) {
                         methods.personalityInsightChatbot(translatedText, logger, mainRes, function (analyzedProfile) {
                             methods.nluChatbot(translatedText, logger, mainRes, function (nluResponse) {
                                 methods.processChatbot(JSON.parse(dueBody)._text, translatedText, analyzedProfile, nluResponse, discoveryResult, personalityCode, logger, mainRes);
                             });
                         });
                     });

                 });
             }
         });
     });
 })
 */