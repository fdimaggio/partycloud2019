const MongoDB = require("../mongoDB")

let nuoveCaseDisponibili = [{
        id: "01",
        name: "Appartamento zona Tortona",
        DIMENSIONI: "media",
        TARGET: "per me 🤙| per coppia",
        BONUS: "con l'ascensore",
        PUNTEGGIO: 0,
        UBICAZIONE: "alla moda 💅",
        ALTEZZA: "",
        DENOMINAZIONE: "bilocale",
    },
    {
        id: "12",
        name: "Trilocale Zona San Siro",
        DIMENSIONI: "grande",
        TARGET: "per coppia 💏",
        BONUS: "con l'ascensore | con mezzi pubblici 🚇 | con il posto auto  🚗",
        PUNTEGGIO: 0,
        UBICAZIONE: "in zona residenziale 🏘️",
        ALTEZZA: "",
        DENOMINAZIONE: "",
    },
    {
        id: "03",
        name: "Appartamento zona Cenisio",
        DIMENSIONI: "media",
        TARGET: "per me 🤙| per coppia",
        BONUS: "con l'ascensore | con il posto bici 🚲",
        PUNTEGGIO: 0,
        UBICAZIONE: "in zona residenziale 🏘️",
        ALTEZZA: "",
        DENOMINAZIONE: "",
    },
    {
        id: "24",
        name: "Attico zona Palestro",
        DIMENSIONI: "grande",
        TARGET: "",
        BONUS: " con l'ascensore  | con il posto bici 🚲 | con mezzi pubblici 🚇",
        PUNTEGGIO: 0,
        UBICAZIONE: "al centro 🖼",
        ALTEZZA: "un attico 🏙",
        DENOMINAZIONE: "un trilocale",
    },
    {
        id: "05",
        name: "Villino zona Sempione",
        DIMENSIONI: "media",
        TARGET: "",
        BONUS: "con il giardino 🌵 | con il posto auto  🚗",
        PUNTEGGIO: 0,
        UBICAZIONE: "",
        ALTEZZA: "un piano terra 🏠",
        DENOMINAZIONE: "un bilocale",
    },
    {
        id: "36",
        name: "Loft zona isola",
        DIMENSIONI: "media",
        TARGET: "per me 🤙| per coppia",
        BONUS: "con il balcone  | con l'ascensore | con la portineria 📫",
        PUNTEGGIO: 0,
        UBICAZIONE: "alla moda 💅",
        ALTEZZA: "un piano alto",
        DENOMINAZIONE: "loft",
    },
    {
        id: "07",
        name: "Bilocale Villa Pizzone",
        DIMENSIONI: "media",
        TARGET: "per coppia 💏 | per anziani 👴👵 | per me 🤙| per famiglia",
        BONUS: "con il balcone | con il posto auto  🚗  | con l'ascensore | con la portineria 📫",
        PUNTEGGIO: 0,
        UBICAZIONE: "in zona residenziale 🏘️",
        ALTEZZA: "un piano terra 🏠",
        DENOMINAZIONE: "un bilocale",
    },
    {
        id: "21",
        name: "Trilocale Corso Magenta",
        DIMENSIONI: "grande",
        TARGET: "per famiglia | per coppia",
        BONUS: "con il posto bici 🚲 | con il posto bici 🚲",
        PUNTEGGIO: 0,
        UBICAZIONE: "al centro 🖼",
        ALTEZZA: "",
        DENOMINAZIONE: "",
    },
    {
        id: "48",
        name: "Monolocale Porta Genova",
        DIMENSIONI: "piccola",
        TARGET: "per me 🤙",
        BONUS: "con la portineria 📫 | con il posto bici 🚲",
        PUNTEGGIO: 0,
        UBICAZIONE: "alla moda 💅",
        ALTEZZA: "un piano terra 🏠",
        DENOMINAZIONE: "un monolocale",
    },
    {
        id: "09",
        name: "Villa a Lecco",
        DIMENSIONI: "grande",
        TARGET: "al lago | per anziani 👴👵 | per coppia 💏",
        BONUS: "con l'entrata separata 🚪 | con il posto auto  🚗 | con il giardino 🌵 | con il balcone",
        PUNTEGGIO: 0,
        UBICAZIONE: "al lago",
        ALTEZZA: "un piano terra 🏠",
        DENOMINAZIONE: "",
    },
    {
        id: "58",
        name: "Casolare in campagna",
        DIMENSIONI: "grande",
        TARGET: "",
        BONUS: "con l'entrata separata",
        PUNTEGGIO: 0,
        UBICAZIONE: "in campagna",
        ALTEZZA: "",
        DENOMINAZIONE: "",
    }
];

module.exports = {
    nuoveCaseDisponibili,
    async ottieniCase() {
        return await MongoDB.retriveHouses();
    },
    verificaCase: async function (caseValorizzate) {
        return new Promise((resolve, reject) => {
            caseValorizzate.sort((a, b) => b.PUNTEGGIO - a.PUNTEGGIO)
            resolve(caseValorizzate)
        })
    },
    assegnaPunteggio: async function (caseInQuestione, categoria, valore, messaggio) {
        return new Promise((resolve, reject) => {
            let epuratedValue = valore.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '')
            caseInQuestione.filter(casa => casa[categoria]).filter(casa => casa[categoria].includes(epuratedValue)).map(casa => {
                casa.PUNTEGGIO++;
                casa.DESCRIZIONE += messaggio
            })
            resolve();
        })
    },
    makeId: function (length) {
        var text = "";
        var possible = "0123456789";
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
}