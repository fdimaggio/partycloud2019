const express = require('express');
const app = express()
let PORT = process.env.PORT || 8080;

const bodyParser = require('body-parser');

// express app setup - bodyParser and Static HTML
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static('public'));

const MessageBuilder = require("./mauden_modules/messageBuilder");
const MsgBuilder = new MessageBuilder();
const Watson = require("./mauden_modules/watsonAssistant");
const MongoDB = require("./mauden_modules/mongoDB");
const Chatfuel = require("./mauden_modules/chatfuel");
const HouseFinder = require("./mauden_modules/houseFinder");
const Emailer = require("./mauden_modules/emailer");

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./openapi.json');

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.post("/webhook", async (req, res) => {
    //let watsonResponse = await Watson.askWatson(messageToWatson, chatfuelResponse.context.token);
    console.time("interaction")
    console.log(req.body)

    let userInput = req.body["last user freeform input"],
        testoUtente = req.body.testo,
        userId = req.body["messenger user id"],
        name = req.body["first name"] + " " + req.body["last name"],
        profilePic = req.body["profile pic url"];

    console.log(`USER_ID: ${userId} dice ${testoUtente}`)

    let chatfuelResponse = {
        messages: []
    }

    let watsonResponse = await Watson.askWatson(userInput, userId, req.body["first name"], req.body["last name"]);

    console.log(watsonResponse)

    for (let singleWatsonOutput of watsonResponse.output.generic) {
        switch (singleWatsonOutput.response_type) {
            case "text":
                switch (singleWatsonOutput.text) {
                    case "%%RICERCA_CASA":
                        //chatfuelResponse.messages.push(MsgBuilder.textMessage("Ora cercherò tra gli immobili di mia conoscenza quello che fa più per te!"));

                        let caseInQuestione = await HouseFinder.ottieniCase();

                        let preferenceRecap = "Ho capito che ti interessano queste caratteristiche:";
                        let {
                            preferences,
                            userText
                        } = watsonResponse.context;

                        for (let singleElement of preferences) {
                            //console.log(singleElement);
                            switch (singleElement.entity) {
                                case "BONUS":
                                    preferenceRecap += `${singleElement.value}, `
                                    //Aggiungere valore di punteggio al singolo appartamento
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "BONUS", singleElement.value, `${singleElement.value}, `)
                                    break;
                                case "ALTEZZA":
                                    preferenceRecap += `situata ad ${singleElement.value}, `
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "ALTEZZA", singleElement.value, `situata ad ${singleElement.value}, `)
                                    break;
                                case "DENOMINAZIONE":
                                    preferenceRecap += `simile ad ${singleElement.value}, `
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "DENOMINAZIONE", singleElement.value, `simile ad ${singleElement.value}, `)
                                    break;
                                case "DIMENSIONI":
                                    preferenceRecap += `una casa ${singleElement.value}, `
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "DIMENSIONI", singleElement.value, `una casa ${singleElement.value}, `)
                                    break;
                                case "TARGET":
                                    preferenceRecap += `comoda ${singleElement.value}, `
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "TARGET", singleElement.value, `comoda ${singleElement.value}, `)
                                    break;
                                case "UBICAZIONE":
                                    preferenceRecap += `possibilmente ${singleElement.value}, `
                                    await HouseFinder.assegnaPunteggio(caseInQuestione, "UBICAZIONE", singleElement.value, `possibilmente ${singleElement.value}, `)
                                    break;
                                default:
                                    break;
                            }
                        }
                        preferenceRecap = preferenceRecap.substring(0, preferenceRecap.length - 2) + ".";
                        chatfuelResponse.messages.push(MsgBuilder.textMessage(preferenceRecap));

                        //Cercare casa
                        let casaScelta = await HouseFinder.verificaCase(caseInQuestione);
                        //console.log(casaScelta)

                        let codiceUtente = HouseFinder.makeId(3) + casaScelta[0].id + HouseFinder.makeId(1);

                        //Salvare utente
                        let loggedUser = await MongoDB.saveUser(name, userId, profilePic, codiceUtente, casaScelta, userText);

                        //Restituire codice della casa
                        chatfuelResponse.messages.push(MsgBuilder.textMessage(`Recati al totem e inserisci questo codice: ✨${codiceUtente}✨`));

                        break;
                    case "%%CHIEDI_AUDIO":
                        chatfuelResponse = {
                            redirect_to_blocks: ["audio"]
                        }
                        break;
                    default:
                        chatfuelResponse.messages.push(MsgBuilder.textMessage(singleWatsonOutput.text));
                        break;
                }

                break;
            case "option":
                //Setup output for options

                let renderingOptions = [];
                singleWatsonOutput.options.map(option => renderingOptions.push(option.label));

                chatfuelResponse.messages.push(MsgBuilder.quickReplyMessage(singleWatsonOutput.title, renderingOptions));
                break;
            default:
                //Setup standard output
                chatfuelResponse.messages.push(MsgBuilder.textMessage("Ci sarebbe qualcosa da visualizzare ma non sono in grado di farlo."));
                break;
        }
    }

    res.send(chatfuelResponse);
})

app.get("/ping", (req, res) => {
    res.send("pong")
})

app.post("/totem/userLogin", async (req, res) => {
    console.log(req.body)
    await Watson.flushConversationByUser(req.body.codice);
    let loggedUser = await MongoDB.retriveUser(req.body.codice);
    console.log("UTENTE LOGGATO")
    res.send(loggedUser);
})

app.post("/totem/createUser", async (req, res) => {
    console.log(req.body);
    let {
        name,
        fbId,
        profilePic,
        codice
    } = req.body;
    let loggedUser = await MongoDB.saveUser(name, fbId, profilePic, codice);
    res.send(loggedUser);
})

app.post("/totem/sendAppointmentData", async (req, res) => {

    //titolo
    //indirizzo

    let loggedUser = await MongoDB.retriveUser(req.body.codice);
    let context = await Watson.totemGetConversationContext(req.body.codice);

    await Chatfuel.sendMessage(loggedUser.fbId, context.data, context.ora, req.body.indirizzo, req.body.titolo);
    await Emailer.sendEmailToCommesso(req.body, loggedUser, context);
    res.send({
        success: true
    })
})

app.post("/totem/message", async (req, res) => {
    let userInput = req.body.message,
        userId = req.body.codice,
        name = req.body.name;

    console.log(req.body)

    let watsonResponse = await Watson.watsonOnTotem(userInput, userId, name);
    console.log("TOTEM: ", {
        codice: userId,
        watson_response: watsonResponse
    })
    res.send({
        codice: userId,
        watson_response: watsonResponse
    });
})
app.get("/totem/flushConversations", async (req, res) => {
    await Watson.flushConversations();
    res.send({
        action: "Flushed Totem conversations"
    });
})

app.listen(PORT, () => {
    console.log(`Server online @ ${PORT}`);
});