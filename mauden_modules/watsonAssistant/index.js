const configs = require('../config')
const AssistantV1 = require('watson-developer-cloud/assistant/v1');

const assistant = new AssistantV1({
    version: configs.Watson.version,
    iam_apikey: configs.Watson.iam_apikey,
    url: configs.Watson.url
});

const totemAssistant = new AssistantV1({
    version: configs.Watson.version,
    iam_apikey: configs.Watson.iam_apikey,
    url: configs.Watson.url
});

//Conversation Context Manager
let conversationContext = new Map();
let totemContext = new Map();

module.exports = {
    askWatson: function (data, userId, nome, cognome) {
        return new Promise((resolve, reject) => {

            let localContext = conversationContext.get(userId) || {
                username: userId,
                nome: nome,
                cognome: cognome
            }

            assistant.message({
                    input: {
                        text: data
                    } || {},
                    context: localContext,
                    workspace_id: configs.Watson.workspace
                },
                function (err, response) {
                    if (err) {
                        console.error(err);
                        reject(err)
                    } else {
                        // saving context for future usage
                        conversationContext.set(userId, response.context);
                        response.output.generic
                        resolve(response);
                    }
                }
            );
        })
    },
    watsonOnTotem: function (data, userId, name) {
        return new Promise((resolve, reject) => {

            let contestoLocale = totemContext.get(userId) || {
                username: userId,
                nome: name
            }

            console.log(contestoLocale != {
                username: userId,
                nome: name
            } ? userId + " - Contesto trovato!" : userId + " - Contesto assente!")

            totemAssistant.message({
                    input: {
                        text: data
                    } || {},
                    context: contestoLocale,
                    workspace_id: configs.Watson.totem_workspace
                },
                function (err, response) {
                    if (err) {
                        console.error(err);
                        reject(err)
                    } else {
                        // saving context for future usage
                        totemContext.set(userId, response.context);
                        resolve(response.output.generic);
                    }
                }
            );
        })
    },
    assistantInstance: assistant,
    flushConversations: function () {
        return new Promise((resolve, reject) => {
            totemContext.clear()
            resolve();
        })
    },
    flushConversationByUser: function (userId) {
        return new Promise((resolve, reject) => {
            totemContext.clear(userId)
            totemContext.delete(userId)
            console.log("Contesto di conversazione cancelato!!")
            resolve();
        })
    },
    totemGetConversationContext: function (userId) {
        return new Promise((resolve, reject) => {
            resolve(totemContext.get(userId))
        })
    }
}