class MessageBuilder {
    constructor() {}

    textMessage(text) {
        return {
            text: text
        }
    }

    quickReplyMessage(text, options) {
        let message = {
            text: text,
            quick_replies: []
        }

        options.map(option => {
            message.quick_replies.push({
                title: option,
                block_names: ["Default Answer"]
            })
        })

        return message;
    }

    verticalReply(text, options) {
        let message = {
            code: 'message',
            type: 'text',
            body: text,
            quick_replies: [],
            quick_replies_orientation: "vertical"
        }

        options.map(option => {
            message.quick_replies.push({
                title: option,
                payload: option,
                content_type: 'text'
            })
        })

        return message;
    }
    
    carouselPreventivi(preventivi) {
        let message = {
            code: 'message',
            type: 'text',
            body: 'Ecco quello che ho trovato per te.',
            template: {
                type: 'generic',
                elements: []
            }
        };

        preventivi.map(singlePreventivo => {
            console.log(singlePreventivo.id)

            let stars = "";
            for (let i of new Array(singlePreventivo.stars)) {
                stars += "⭐"
            }

            message.template.elements.push({
                title: singlePreventivo.title,
                image_url: singlePreventivo.imgLink,
                subtitle: `${stars} - Per te a soli ${singlePreventivo.price}€ - ${singlePreventivo.description.substring(0, 50)}...`,
                default_action: {
                    type: 'web_url',
                    url: singlePreventivo.link
                },
                buttons: [{
                        type: 'web_url',
                        title: 'Mostra dettagli',
                        url: singlePreventivo.link
                    },
                    {
                        type: 'payload',
                        title: 'Scegli questo',
                        payload: singlePreventivo.id
                    }
                ]
            })
        })

        return message;
    }

}

module.exports = MessageBuilder;