const SparkPost = require('sparkpost');
const sparky = new SparkPost('0f4d9f1f4156066229feff2ea0e57e0305a5172a');

module.exports = {
    sendEmailToCommesso: function (body, loggedUser, context) {
        return new Promise((resolve, reject) => {
            sparky.transmissions.send({
                    options: {
                        sandbox: false
                    },
                    content: {
                        from: '<AwesHome>aweshome@mauden.com',
                        subject: `${body.codice} - Nuovo appuntamento richiesto!`,
                        html: `<div>
                            <h2>Nuova richiesta di Appuntamento</h2>
                            <h4>AwesHome - Utente ${body.codice}</h4>
                            <p>L'utente ha richiesto un nuovo apuntamento.</p>
                        </div>
                        <div>
                            <table>
                                <thead>
                                    <tr>
                                        <td width="150"></td>
                                        <td width="300"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Contatto Utente</td>
                                        <td>${loggedUser.name}</td>
                                    </tr>
                                    <tr>
                                        <td>Codice Utente</td>
                                        <td>${body.codice}</td>
                                    </tr>
                                    <tr>
                                        <td>Data e ora</td>
                                        <td>${context.data} alle ${context.ora}</td>
                                    </tr>
                                    <tr>
                                        <td>Casa proposta all'utente</td>
                                        <td>La casa proposta è <b>${loggedUser.houses[0].name} - ${loggedUser.houses[0].INDIRIZZO}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Casa scelta dall'utente</td>
                                        <td>La casa scelta è <b>${body.titolo} - ${body.indirizzo}</b></td>
                                    </tr>
                                    <tr>
                                        <td>Descrizione abitazione dell'utente</td>
                                        <td>${loggedUser.userText}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <hr>`
                    },
                    recipients: [{
                        address: 'Commesso.aweshome@gmail.com'
                    }]
                })
                .then(data => {
                    console.log("EMAIL_SENDER: Invio email completato")
                    resolve();
                })
                .catch(err => {
                    console.log("EMAIL_SENDER: Invio email ERRATO!!!!")
                    resolve();
                });
        });
    }
}